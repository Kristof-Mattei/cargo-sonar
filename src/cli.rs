use clap::Parser;
use eyre::bail;
use std::path::PathBuf;

#[derive(Debug)]
pub enum Issue {
    #[cfg(feature = "audit")]
    Audit,
    #[cfg(feature = "clippy")]
    Clippy,
    #[cfg(feature = "deny")]
    Deny,
    #[cfg(feature = "outdated")]
    Outdated,
    #[cfg(feature = "udeps")]
    Udeps,
}

impl std::str::FromStr for Issue {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let issue = match s.to_lowercase().as_str() {
            "audit" => Issue::Audit,
            "clippy" => Issue::Clippy,
            "deny" => Issue::Deny,
            "outdated" => Issue::Outdated,
            "udeps" => Issue::Udeps,
            _ => bail!("issue '{}' is not supported.", s),
        };
        Ok(issue)
    }
}

#[derive(Debug)]
pub enum Coverage {
    #[cfg(feature = "tarpaulin")]
    Tarpaulin,
}

impl std::str::FromStr for Coverage {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let coverage = match s.to_lowercase().as_str() {
            "tarpaulin" => Coverage::Tarpaulin,
            _ => bail!("coverage '{}' is not supported.", s),
        };
        Ok(coverage)
    }
}

#[derive(Debug, Parser)]
#[clap(version, author)]
pub struct Command {
    #[clap(short, long, value_delimiter = ',')]
    pub issues: Vec<Issue>,
    #[clap(short, long)]
    pub coverage: Coverage,
    #[cfg(feature = "audit")]
    #[clap(long, default_value = "audit.json")]
    pub audit_path: PathBuf,
    #[cfg(feature = "clippy")]
    #[clap(long, default_value = "clippy.json")]
    pub clippy_path: PathBuf,
    #[cfg(feature = "deny")]
    #[clap(long, default_value = "deny.json")]
    pub deny_path: PathBuf,
    #[cfg(feature = "outdated")]
    #[clap(long, default_value = "outdated.json")]
    pub outdated_path: PathBuf,
    #[cfg(feature = "tarpaulin")]
    #[clap(long, default_value = "tarpaulin-report.json")]
    pub tarpaulin_path: PathBuf,
    #[cfg(feature = "udeps")]
    #[clap(long, default_value = "udeps.json")]
    pub udeps_path: PathBuf,
    #[clap(long, default_value = "sonar-issues.json")]
    pub issues_path: PathBuf,
    #[clap(long, default_value = "sonar-coverage.xml")]
    pub coverage_path: PathBuf,
}
