mod coverage;
pub use coverage::{Coverage, File, LineToCover};
mod issue;
pub use issue::{Issue, Issues, Location, Severity, TextRange, Type};
